'use strict';

pokemonApp.controller('PokemonListCtrl', function($scope, PokemonsService) {

	$scope.pokemonsLoaded = false;

    $scope.pokemons = PokemonsService.query(function() {
        // Окей!
        $scope.pokemonsLoaded = true;
    });

});
